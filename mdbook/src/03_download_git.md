# Download Git

To download the latest version of git for windows go to: [http://gitforwindows.org/](http://gitforwindows.org/)

Once the download has completed start the **Git Setup** and follow the prompts as shown below...

![](assets/Git_Setup_2_29_2_2_Paint1.png)

![](assets/Git_Setup_2_29_2_2_Paint2.png)

![](assets/Git_Setup_2_29_2_2_Paint3.png)

![](assets/Git_Setup_2_29_2_2_Paint4.png)

![](assets/Git_Setup_2_29_2_2_Paint7.png)

![](assets/Git_Setup_2_29_2_2_Paint8.png)

![](assets/Git_Setup_2_29_2_2_Paint9.png)

![](assets/Git_Setup_2_29_2_2_Paint10_Choosing_HTTPS_Transport_backend)

![](assets/Git_Setup_2_29_2_2_Paint11_ConfiguringTheLineEndingConversions)

![](assets/Git_Setup_2_29_2_2_Paint12_ConfiguringTheTerminalEmulatorToUseWithGitBash)

![](assets/Git_Setup_2_29_2_2_Paint13_ChooseTheDefaultBehaviorOfGitPull)

![](assets/Git_Setup_2_29_2_2_Paint14_ChooseACredentialHelper)

![](assets/Git_Setup_2_29_2_2_Paint15_ConfiguringExtraOptions)

![](assets/Git_Setup_2_29_2_2_Paint16_ConfiguringExperimentalOptions)

![](Git_Setup_2_29_2_2_Git_Setup_2_29_2_2_Paint17_Install)

![](assets/Git_Setup_2_29_2_2_Paint18_CompletingTheGitSetupWizard)

 Run the following commands to configure your **Git username **and **email.** These details will be associated with any commits that you create:

![](assets/27.PNG)
